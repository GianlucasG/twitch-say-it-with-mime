import TwitchMessage from '../components/TwitchMessage'
import Words from '../components/Words'
import { useState, useEffect, useRef } from 'react';
function HomePage() {

  const [isConnected, setIsConnected] = useState(false);

  const handleConnect = () => {
    setIsConnected(true);
  };

  return (
    <div className='home-container'>
      <div className="words-container">
        <Words />
        <button onClick={handleConnect} disabled={isConnected}>
          Connect to Twitch
        </button>
      </div>
      <div className="users-container">
        {isConnected && <TwitchMessage />}
      </div>
    </div>
  )
}

export default HomePage