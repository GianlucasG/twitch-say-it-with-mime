import React, { useState, useEffect } from 'react';

const Timer = ({ isGameStarted, onTimerEnd }) => {
  const [timer, setTimer] = useState(30);

  useEffect(() => {
    const countdown = setInterval(() => {
      if (isGameStarted && timer > 0) {
        setTimer((prevTimer) => prevTimer - 1);
      } else if (timer === 0 && isGameStarted) {
        clearInterval(countdown);
        onTimerEnd();
      }
    }, 1000);

    return () => clearInterval(countdown);
  }, [isGameStarted, timer, onTimerEnd]);

  return <p>Time remaining: {timer} seconds</p>;
};

export default Timer;