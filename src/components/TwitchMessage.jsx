import { useEffect,useState } from 'react';

const TwitchMessage = () => {
  const [twitch_message, setTwitchMessage] = useState([]);
  const [usernameColor, setUsernameColor] = useState('');
  const [username, setUsername] = useState('');
  useEffect(() => {
    // Set the correct answer
    const correctAnswer = 'd';

    // Initialize TMI client
    const client = new tmi.Client({
      channels: ['elspreen'],
    });

    // Listen for incoming messages
    client.on('message', (channel, userstate, message, self) => {
      if (!self) {
        const { username, color } = userstate;
        setTwitchMessage(message);
        setUsernameColor(color);
        setUsername(username);
        // Check if the message is the correct answer
        if (message.toLowerCase() === correctAnswer.toLowerCase()) {
          // Disconnect the client when the correct answer is received
          client.disconnect();
        }
      }
    });

    // Connect to Twitch
    client.connect();

    // Cleanup function
    return () => {
      client.disconnect();
    };
  }, []);

  return (
    <div>
      <span style={{ color: usernameColor }}>{username}:</span> {twitch_message}
    </div>
  );
};

export default TwitchMessage;