import { useState, useEffect, useRef } from 'react';
function Words() {

  const [stringArray, setStringArray] =useState(['hola','chau']);
  const inputRef = useRef(null);

  useEffect(() => {
    sessionStorage.setItem('stringArray', JSON.stringify(stringArray))
  }, [stringArray]);

  const handleAddWord = () => {
    const newWord = inputRef.current.value.trim();
    
    if (newWord === '') {
      return;
    }
  
    const updatedArray = [...stringArray, newWord];

    setStringArray(updatedArray);
    sessionStorage.setItem('stringArray', JSON.stringify(updatedArray));
  
    inputRef.current.value = '';
    console.log(sessionStorage.getItem('stringArray'));
  }

  return (
    <div>
      <input type="text" ref={inputRef} id="inputString"/>
      <button onClick={handleAddWord}>
        Add new word
      </button>
    </div>
  )
}

export default Words